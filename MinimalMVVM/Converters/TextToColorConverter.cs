﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace MinimalMVVM.Converters
{
    public class TextToColorConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                if(stringValue.Any(x => char.IsDigit(x)))
                {
                    return (stringValue.Any(x => char.IsLetter(x))) ? Brushes.Yellow : Brushes.Green;
                }
                else
                {
                    return Brushes.Red;
                }
            }
            return Colors.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
