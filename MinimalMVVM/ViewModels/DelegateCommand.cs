﻿using System;
using System.Windows.Input;

namespace MinimalMVVM.ViewModels
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _action;
        private readonly Action<object> _actionWithParam;

        public DelegateCommand(Action action)
        {
            _action = action;
        }

        public DelegateCommand(Action<object> action)
        {
            _actionWithParam = action;
        }

        public void Execute(object parameter)
        {
            if(parameter != null)
            {
                _actionWithParam(parameter);
            }
            else
            {
                _action();
            }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

#pragma warning disable 67
        public event EventHandler CanExecuteChanged { add { } remove { } }
#pragma warning restore 67
    }
}
