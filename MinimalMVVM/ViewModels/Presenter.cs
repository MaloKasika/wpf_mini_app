﻿using MinimalMVVM.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace MinimalMVVM.ViewModels
{
    public class Presenter : ObservableObject
    {
        private string _someText;
        private int _historyCount;

        private readonly TextConverter _textConverter = new TextConverter(s => s.ToUpper());
        private readonly TextConverter _prependConverter = new TextConverter(s => "-" + s);
        private readonly ObservableCollection<string> _history = new ObservableCollection<string>();

        public IEnumerable<string> History => _history;

        public ICommand ConvertTextCommand => new DelegateCommand(ConvertText);

        public ICommand PrependDashCommand => new DelegateCommand(PrependDash);

        public ICommand RestoreLastItemCommand => new DelegateCommand(RestoreLastItem);

        public ICommand RestoreSelectedItemCommand => new DelegateCommand(RestoreSelectedItem);

        public string SomeText
        {
            get => _someText;
            set
            {
                _someText = value;
                RaisePropertyChangedEvent(nameof(SomeText));
            }
        }

        public int HistoryCount
        {
            get => _historyCount;
            set
            {
                _historyCount = value;
                RaisePropertyChangedEvent(nameof(HistoryCount));
            }
        }

               
        private void ConvertText()
        {
            if (string.IsNullOrWhiteSpace(SomeText))
            {
                return;
            }

            AddToHistory(_textConverter.ConvertText(SomeText));
            SomeText = string.Empty;
        }

        private void PrependDash()
        {
            if (string.IsNullOrWhiteSpace(SomeText))
            {
                return;
            }

            AddToHistory(_prependConverter.ConvertText(SomeText));
            SomeText = string.Empty;
        }

        private void RestoreLastItem()
        {
            if (_historyCount > 0)
            {
                SomeText = RevertHistoryText(_history.Last());
                _history.RemoveAt(_historyCount - 1);
                HistoryCount--;
            }
        }

        private void RestoreSelectedItem(object item)
        {
            if (item is string itemToRestore)
            {
                var itemIndex = _history.IndexOf(itemToRestore);
                if (itemIndex >= 0)
                {
                    SomeText = RevertHistoryText(_history[itemIndex]);
                    _history.RemoveAt(itemIndex);
                    HistoryCount--;
                }
            }
        }

        private void AddToHistory(string item)
        {
            if (!_history.Contains(item))
            {
                _history.Add(item);
                HistoryCount++;
            }
        }

        private string RevertHistoryText(string text)
        {
            if (text.StartsWith("-"))
            {
                text = text.Remove(0, 1);
            }
            return text.ToLower();
        }

        

    }
}
